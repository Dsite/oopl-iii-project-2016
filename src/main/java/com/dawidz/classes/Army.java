package com.dawidz.classes;

import com.dawidz.interfaces.State;
/**
 * Created by Dside on 1/21/2017.
 */
public abstract class Army implements State {
    protected String Nation;
    protected String Colors;
    protected int NumberOfUnits;
    protected int UnitsInUsing;
    protected int UnitFree;
    protected int Budget;
    protected String State;


    public Army() {
        this.Nation = "UnitedStates";
        this.Colors = "WhiteAndBlack";
        this.NumberOfUnits = 32543;
        this.UnitsInUsing = 324;
        this.UnitFree = this.NumberOfUnits - this.UnitsInUsing;
        this.Budget = 432543;
        this.State = "defense";


    }

    public Army(String nation, String colors, int numberOfUnits, int unitInUsing, int unitFree, int budget, String state) {

        this.Nation = nation;
        this.Colors = colors;
        this.NumberOfUnits = numberOfUnits;
        this.UnitsInUsing = unitInUsing;
        this.UnitFree = unitFree;
        this.Budget = budget;
        this.State = state;
    }

    public void setNation(String nation) {
        Nation = nation;
    }

    public void setColors(String colors) {
        Colors = colors;
    }

    public void setNumberOfUnits(int numberOfUnits) {
        NumberOfUnits = numberOfUnits;
    }

    public void setUnitsInUsing(int unitsInUsing) { UnitsInUsing = unitsInUsing;  }

    public void setUnitFree(int unitFree) {
        UnitFree = unitFree;
    }

    public void setBudget(int budget) {
        Budget = budget;
    }

    public void setState(String state) { State = state;  }

    public String getNation() { return Nation; }

    public String getColors() {
        return Colors;
    }

    public int getNumberOfUnits() {
        return NumberOfUnits;
    }

    public int getUnitsInUsing() {
        return UnitsInUsing;
    }

    public int getUnitFree() {
        return UnitFree;
    }

    public int getBudget() {
        return Budget;
    }

    public String getState() { return State; }

    public void callWar() {
        //***********************feature is wait ***************
    }

    public void callBackFromWar() {
        //***********************feature is wait ***************
    }


    public void attack(){
        System.out.println("\nYour Army are attacking the enemy!");


    }

    public  void defense(){
        System.out.println("\nYour Army are set to defense");
    }

    // @Override
    // public String serializeToJson(){}

}
