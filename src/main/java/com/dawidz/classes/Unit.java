package com.dawidz.classes;

import com.dawidz.interfaces.Shop;
import com.dawidz.exceptions.NotEnoughMoneyException;
import com.dawidz.exceptions.NonUnitsException;

/**
 * Created by Dside on 1/21/2017.
 */
public class Unit extends Army implements Shop{
    public Unit(){
        super();
    }

    public Unit(String nation, String colors, int numberOfUnits, int unitsInUsing, int unitFree, int budget, String state){
    super(nation, colors, numberOfUnits, unitsInUsing, unitFree, budget, state);

    }
    @Override
    public void attack(){
        System.out.println("\nYour Units go attack");
    }

    public void defense(){
        System.out.println("\nYour Units are set to defense");
    }

    public void buy(String type, int value)   {
       if(getBudget()>value){
           setNumberOfUnits(getNumberOfUnits()+value);
           System.out.println("\nYou bought new "+type+" x"+value);
           System.out.println("\n[log] budget is: "+getBudget());
       }else{
           try{
               throw new NotEnoughMoneyException();
           }catch (NotEnoughMoneyException ex){
               System.out.println("[log]You cant buy "+value+"x"+type+" - You have only "+getBudget()+"$");
               ex.getMessage();
               ex.printStackTrace();
               return;
           }
       }
    }

    public void sell(String type, int value) {
        if(getUnitFree()>0 && value<= getNumberOfUnits()){
            setNumberOfUnits(getNumberOfUnits()-value);
            System.out.println("\nYou sold "+type+" x"+value);
        }else{
            try{
                throw new NonUnitsException();
            }catch (NonUnitsException ex){
                System.out.println("[log]You cant sell "+value+"x"+type+" - You have only "+getNumberOfUnits());
                ex.getMessage();
                ex.printStackTrace();
                return;
            }
        }
    }
}
