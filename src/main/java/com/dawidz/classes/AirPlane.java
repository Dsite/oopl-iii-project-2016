package com.dawidz.classes;

/**
 * Created by Dside on 1/21/2017.
 */
public class AirPlane extends Vechicle {
    public AirPlane() {
        super();
    }

    public AirPlane(String nation, String colors, int numberOfUnits, int unitsInUsing, int unitFree, int budget, String state) {
        super(nation, colors, numberOfUnits, unitsInUsing, unitFree, budget, state);
    }

    @Override
    public void attack() {
        System.out.println("\nYour AirPlanes go to attacking");
    }
}
