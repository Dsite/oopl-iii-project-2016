package com.dawidz.classes;

/**
 * Created by Dside on 1/21/2017.
 */
public class SniperSoldier extends Soldiers {
    public SniperSoldier() {
        super();
    }

    public SniperSoldier(String nation, String colors, int numberOfUnits, int unitsInUsing, int unitFree, int budget, String state) {
        super(nation, colors, numberOfUnits, unitsInUsing, unitFree, budget, state);
    }

    @Override
    public void attack() {
        System.out.println("\nYour Snipers go head shooting");
    }
}
