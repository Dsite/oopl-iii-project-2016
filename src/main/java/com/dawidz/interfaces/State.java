package com.dawidz.interfaces;

/**
 * Created by Dside on 1/21/2017.
 */
public interface State {
    void defense();
    void attack();
}
