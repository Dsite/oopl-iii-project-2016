package com.dawidz.interfaces;

/**
 * Created by Dside on 1/22/2017.
 */
public interface Shop{
    void buy(String type, int value);
    void sell(String type, int value);
}
