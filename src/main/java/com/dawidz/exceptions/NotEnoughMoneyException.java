package com.dawidz.exceptions;

/**
 * Created by Dside on 1/22/2017.
 */
public class NotEnoughMoneyException extends Exception {
    @Override
    public String getMessage(){
        return "[error]You have no enough money!";
    }
}
