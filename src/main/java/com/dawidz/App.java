package com.dawidz;

import com.dawidz.classes.*;

import java.util.*;

/**
 * Created by Dawid Ziółkowski @ 2017
 *
 */
public class App 
{
    public static void main( String[] args ){


        System.out.println("*ARMYs SOFTWARE\n*YOU CAN MENAGE YOUR ARMY\n*by DAWID ZIOLKOWSKI 2016");
        List<Unit> unitsList = new LinkedList<Unit>(
                Arrays.asList(
                        new Tank("USA","WhiteAndBlack", 2, 0,2,5,"defence"),
                        new AirPlane("USA", "LightGrayMoro",3,1,2,7,"defence"),
                        new SniperSoldier("RUS", "DarkGreyMoro", 5,3,2,15,"research"),
                        new HeavySolier("PL", "DarkGreenMoro", 10,6,4,20,"attack"),
                        new ScoutSoldier("FR", "DarkBlueMoro", 8,3,5,4,"research")));
        int value=0;
        HashMap<Integer, Unit> unitsHashMap = new LinkedHashMap<Integer, Unit>();
        for (Unit unit:unitsList ) {
            unitsHashMap.put(value,unit);
            value++;
        }
        System.out.println("\n\n\tAll Your army units:");
        for (Integer count:unitsHashMap.keySet()) {
            System.out.println("|["+count+"]nation:"
                    +unitsHashMap.get(count).getNation()+" \tcolors:"
                    +unitsHashMap.get(count).getColors()+" \t\tnumberOfUnits:"
                    +unitsHashMap.get(count).getNumberOfUnits()+" \tstate:"
                    +unitsHashMap.get(count).getState());
        }
        System.out.println("\n\n\tTest of methods, exceptions:");
        unitsHashMap.get(2).buy("Tank", 1);
        unitsHashMap.get(2).buy("Scout", 20);
        unitsHashMap.get(4).sell("Sniper", 2);
        unitsHashMap.get(4). sell("AirPlane", 20);

    }
}
